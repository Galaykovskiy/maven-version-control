package com.kulish.mavenversioncontrol.view;

import javafx.scene.control.ButtonType;

public class Alert {
    public static void show(String text) {
        javafx.scene.control.Alert alert = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setHeaderText(text);

        ButtonType okayButton = new ButtonType("Okay");
        alert.getButtonTypes().setAll(okayButton);

        alert.showAndWait();
    }
}

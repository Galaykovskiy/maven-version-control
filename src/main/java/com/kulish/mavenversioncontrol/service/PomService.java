package com.kulish.mavenversioncontrol.service;

import com.kulish.mavenversioncontrol.entities.PomEntity;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

public class PomService {
    private static PomService instance;

    public static PomService getInstance() {
        if (instance == null)
            instance = new PomService();
        return instance;
    }

    private PomService() {
    }

    public void readPomFile(File pomFile) {
        readPomFile(pomFile.getPath());
    }

    public void readPomFile(String filePath) {
        try {
            Reader reader = new FileReader(filePath);
            MavenXpp3Reader xpp3Reader = new MavenXpp3Reader();
            Model model = xpp3Reader.read(reader);
            EntityService.getInstance().addPomEntity(filePath, model);
            EntityService.getInstance().updateEntities();

            reader.close();
        } catch (XmlPullParserException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writePomFile(PomEntity pomEntity) {
        MavenXpp3Writer writer = new MavenXpp3Writer();
        try (FileWriter fileWriter = new FileWriter(pomEntity.getFilePath())) {
            writer.write(fileWriter, pomEntity.getModel());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

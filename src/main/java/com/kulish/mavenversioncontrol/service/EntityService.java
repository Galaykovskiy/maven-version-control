package com.kulish.mavenversioncontrol.service;

import com.kulish.mavenversioncontrol.entities.PomEntity;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.maven.model.Model;

public class EntityService {
    private static EntityService instance;

    // Map<artifactId, PomEntity>
    private final Map<String, PomEntity> pomEntities;
    private PomEntity currentPomEntity;

    private EntityService() {
        pomEntities = new HashMap<>();
    }

    public static EntityService getInstance() {
        if (instance == null) {
            instance = new EntityService();
        }
        return instance;
    }

    public void addPomEntity(String filePath, Model model) {
        PomEntity pomEntity = new PomEntity(filePath, model);

        pomEntities.put(model.getArtifactId(), pomEntity);
    }

    public int getCount() {
        return pomEntities.size();
    }

    public void removePomEntity(String artifactId) {
        pomEntities.get(artifactId).remove();
        pomEntities.remove(artifactId);
        removePomChildrenProperties(artifactId);
        updateEntities();
    }

    private void removePomChildrenProperties(String artifactId) {
        pomEntities.values().forEach(c -> c.removeChildrenProperty(artifactId));
    }

    public PomEntity getPomEntity(String artifactId) {
        return pomEntities.get(artifactId);
    }

    public boolean isPomFileExisting(File file) {
        return !pomEntities.values().stream()
                .filter(p -> p.getFilePath().equals(file.getPath()))
                .collect(Collectors.toSet())
                .isEmpty();
    }

    public void updateEntities() {
        pomEntities.values().forEach(PomEntity::updateEntity);
    }

    @Deprecated
    public void updateEntitiesVersion() {
        pomEntities.values().forEach(PomEntity::updateEntityVersion);
    }

    public void updateGroups() {
        pomEntities.values().forEach(PomEntity::updateGroups);
    }

    public Set<String> getPomArtifactIds() {
        return pomEntities.keySet();
    }

    public void saveAllChanges() {
        pomEntities.values().forEach(pom -> {
            pom.saveChanges();
            PomService.getInstance().writePomFile(pom);
        });
    }

    public PomEntity getCurrentPomEntity() {
        return currentPomEntity;
    }

    public void setCurrentPomEntity(PomEntity pomEntity) {
        currentPomEntity = pomEntity;
    }
}
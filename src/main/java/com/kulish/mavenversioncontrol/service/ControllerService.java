package com.kulish.mavenversioncontrol.service;

import com.kulish.mavenversioncontrol.controllers.IController;
import com.kulish.mavenversioncontrol.controllers.MainViewController;
import com.kulish.mavenversioncontrol.controllers.tabs.ChangelogTabViewController;
import com.kulish.mavenversioncontrol.controllers.tabs.FilesTabViewController;
import com.kulish.mavenversioncontrol.controllers.tabs.VersionEditorTabViewController;
import java.util.HashMap;
import java.util.Map;

public class ControllerService<T extends IController> {
    private static ControllerService<IController> instance;
    private final Map<Class<?>, T> controllers;

    private ControllerService() {
        controllers = new HashMap<>();
    }

    public <K extends T> K getController(Class<K> cls) {
        T controller = controllers.get(cls);
        if (cls.isInstance(controller)) {
            return cls.cast(controller);
        } else {
            return null;
        }
    }

    public void addController(Class<?> cls, T controller) {
        controllers.put(cls, controller);
    }

    public static ControllerService<IController> getInstance() {
        if (instance == null) {
            instance = new ControllerService<>();
        }
        return instance;
    }

    public static class Controllers {

        public static FilesTabViewController getFilesTabViewController() {
            return ControllerService.getInstance().getController(FilesTabViewController.class);
        }

        public static MainViewController getMainViewController() {
            return ControllerService.getInstance().getController(MainViewController.class);
        }

        public static VersionEditorTabViewController getVersionTabViewController() {
            return ControllerService.getInstance().getController(VersionEditorTabViewController.class);
        }

        public static ChangelogTabViewController getChangelogTabViewController() {
            return ControllerService.getInstance().getController(ChangelogTabViewController.class);
        }
    }
}

package com.kulish.mavenversioncontrol.service;

import com.kulish.mavenversioncontrol.enums.PanelColorsEnum;
import java.util.HashMap;
import java.util.Map;

public class GroupsService {
    private static GroupsService instance;
    // Key - group name
    // Value - string color in hex "#cccccc"
    private final Map<String, String> groups = new HashMap<>();

    public static final String DEFAULT_GROUP = "NONE";

    private GroupsService() {
        groups.put(DEFAULT_GROUP, PanelColorsEnum.GRAY.getHexCode());
    }

    public Map<String, String> getGroups() {
        return groups;
    }

    public static GroupsService getInstance() {
        if (instance == null)
            instance = new GroupsService();
        return instance;
    }
}

package com.kulish.mavenversioncontrol.entities;

import com.kulish.mavenversioncontrol.controllers.PropertyViewController;
import com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum;
import com.kulish.mavenversioncontrol.service.EntityService;
import com.kulish.mavenversioncontrol.utils.Version;
import static com.kulish.mavenversioncontrol.utils.Version.compareVersions;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

public class PropertyEntity {
    private final String artifactId;
    private final Version oldVersion;
    private Version newVersion;

    private HBox propertyContainer;
    private PropertyViewController propertyController;

    private boolean isLocked;

    public PropertyEntity(String artifactId, String version) {
        this.artifactId = artifactId;
        oldVersion = new Version(version);
        newVersion = new Version(version);
        addController();
        updateVersion(newVersion);
    }

    private void addController() {
        try {
            FXMLLoader loader = new FXMLLoader(PropertyViewController.class.getResource("property-view.fxml"));
            propertyContainer = loader.load();
            propertyController = loader.getController();
            propertyController.setArtifactId(artifactId);
            propertyController.setOldVersion(oldVersion.toString());
            propertyController.setOnLockButtonHandler(this::onLockButtonHandler);
            propertyController.updateLockButtonGraphic(isLocked);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void onLockButtonHandler(MouseEvent event) {
        isLocked = !isLocked;
        propertyController.updateLockButtonGraphic(isLocked);
        updateVersion(newVersion);
//        TODO implementation
        EntityService.getInstance().getPomEntity(artifactId).updateEntityVersion();
    }

    public void updateVersion(Version newVersion) {
        this.newVersion = newVersion;
        propertyController.setNewVersion(newVersion.toString());

        ComparisonStatementEnum result = compareVersions(oldVersion, newVersion);
        if (isLocked && result != ComparisonStatementEnum.EQUAL) {
            propertyController.setStrickenState();
        } else {
            propertyController.setNormalState(result);
        }
    }

    /**
     * @return true if version is updated, but should be false if is locked
     */
    public boolean isVersionUpdatedOrLocked() {
        return !isLocked && !oldVersion.toString().equals(newVersion.toString());
    }

    public Node getNode() {
        return propertyContainer;
    }

    public void setLockButtonDisable(boolean isDisable) {
        propertyController.setLockButtonDisable(isDisable);
    }

    public String getArtifactId() {
        return artifactId;
    }

    public String getOldVersion() {
        return oldVersion.toString();
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void saveChanges() {
        if (isLocked)
            return;
        oldVersion.setVersion(newVersion);
        propertyController.setOldVersion(oldVersion.toString());
        updateVersion(oldVersion);
    }
}

package com.kulish.mavenversioncontrol.entities;

import com.kulish.mavenversioncontrol.controllers.PomElementViewController;
import com.kulish.mavenversioncontrol.controllers.tabs.FileViewController;
import com.kulish.mavenversioncontrol.controllers.tabs.LogElementViewController;
import com.kulish.mavenversioncontrol.enums.UpdateVersionStatementEnum;
import com.kulish.mavenversioncontrol.service.EntityService;
import com.kulish.mavenversioncontrol.utils.Version;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.apache.maven.model.Model;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.kulish.mavenversioncontrol.service.ControllerService.Controllers.*;
import static com.kulish.mavenversioncontrol.service.GroupsService.DEFAULT_GROUP;

/**
 * Contains all necessary fields
 */
public class PomEntity {
    private final String filePath;
    private final Model model;

    private HBox fileViewContainer;
    private FileViewController fileViewController;

    private VBox pomContainer;
    private PomElementViewController pomController;

    private final String artifactId;
    private final Version oldVersion;
    private final Version newVersion;

    private final Map<String, PropertyEntity> presentProperties;
    private final Map<String, PropertyEntity> absentProperties;
    private final Map<String, PropertyEntity> childrenProperty;

    private HashSet<LogElementViewController> logs;

    private MovablePomEntity movablePomEntity;

    // true when node version was changed, parent pom excluded
    private boolean isChanged;
    private String group;

    public PomEntity(String filePath, Model model) {
        this.filePath = filePath;
        this.model = model;
        artifactId = model.getArtifactId();
        oldVersion = new Version(model.getVersion());
        newVersion = new Version(model.getVersion());

        presentProperties = new HashMap<>();
        absentProperties = new HashMap<>();
        childrenProperty = new HashMap<>();

        logs = new HashSet<>();

        group = DEFAULT_GROUP;

        try {
            addPomToMainPanel();
            addPomToFileList();
            addProperties();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        movablePomEntity = new MovablePomEntity(pomController);
    }

    private void addProperties() {
        model.getProperties().forEach((artifact, version) -> {
            if (!artifact.toString().endsWith(".version")) return;
            String artifactId = artifact.toString().replace(".version", "");
            PomEntity pomEntity = EntityService.getInstance().getPomEntity(
                    artifactId);

            if (pomEntity == null) {
                absentProperties.put(artifactId, createProperty(artifactId, version.toString()));
            } else {
                PropertyEntity property = createProperty(artifactId, version.toString());
                presentProperties.put(artifactId, property);
                EntityService.getInstance().getPomEntity(artifactId).addChildrenProperty(this.artifactId, property);
            }
        });
        presentProperties.values().forEach(c -> {
            pomController.addPropertyToPresentNode(c.getNode());
            c.setLockButtonDisable(false);
        });
        absentProperties.values().forEach(c -> {
            pomController.addPropertyToAbsentNode(c.getNode());
            c.setLockButtonDisable(true);
        });
    }

    private PropertyEntity createProperty(String artifactId, String version) {

        return new PropertyEntity(artifactId.replace(".version", ""), version);
    }

    private void addPomToFileList() throws IOException {
        FXMLLoader loader = new FXMLLoader(FileViewController.class.getResource("file-view.fxml"));
        fileViewContainer = loader.load();
        getFilesTabViewController().addContainer(fileViewContainer);
        fileViewController = loader.getController();
        fileViewController.setArtifactId(artifactId);
        fileViewController.setOldVersion(oldVersion.toString());
    }

    private void addPomToMainPanel() throws IOException {
        FXMLLoader loader = new FXMLLoader(PomElementViewController.class.getResource("pom-element-view.fxml"));
        pomContainer = loader.load();
        getMainViewController().addContainer(pomContainer);
        pomContainer.setLayoutY(20);
        pomContainer.setLayoutX(50 + 400 * (EntityService.getInstance().getCount()));

        pomController = loader.getController();
        pomController.setArtifactId(artifactId);
        pomController.setOldVersion(oldVersion);
        pomController.setBackgroundColor(group);
        pomController.setGroupDropDownValue(group);
        pomController.setGroupDropDownHandler(this::setOnGroupDropDownActionHandler);
        pomController.addVersionNodeHandler(this::setOnVersionNodeMouseClickHandler);
    }

    private void setOnGroupDropDownActionHandler(ActionEvent event) {
        group = pomController.getGroupDropDownValue() == null ? DEFAULT_GROUP : pomController.getGroupDropDownValue();
        pomController.setGroupDropDownValue(group);
        pomController.setBackgroundColor(group);
    }

    private void setOnVersionNodeMouseClickHandler(MouseEvent event) {
        // Version Tab
        getVersionTabViewController().setVisible(true);
        getVersionTabViewController().setArtifactIdText(artifactId);
        getVersionTabViewController().updateVersion(oldVersion, newVersion);
        getVersionTabViewController().setApplyButtonHandler(this::setOnApplyMouseButtonHandler);
        getVersionTabViewController().setResetButtonHandler(this::setOnResetMouseButtonHandler);
        getMainViewController().selectVersionTab();

        // Changelog tab
        if (Version.verifyVersionPattern(oldVersion)) {
            getChangelogTabViewController().setVisible(true);
            getChangelogTabViewController().setArtifactId(artifactId);
            getChangelogTabViewController().setVersion(newVersion);
            getChangelogTabViewController().updateScrollPane(logs);
        } else {
            getChangelogTabViewController().setVisible(false);
        }

        EntityService.getInstance().setCurrentPomEntity(this);
    }

    private void setOnApplyMouseButtonHandler(MouseEvent event) {
        isChanged = true;
        newVersion.setVersion(getVersionTabViewController().getVersion());
        getVersionTabViewController().updateVersion(oldVersion, newVersion);
        updateEntityVersion();
    }

    private void setOnResetMouseButtonHandler(MouseEvent event) {
        isChanged = false;
        newVersion.setVersion(oldVersion.toString());
        getVersionTabViewController().updateVersion(oldVersion, newVersion);
        updateEntityVersion();
    }

    public void updateVersionByState(UpdateVersionStatementEnum statement) {
        this.newVersion.updateVersion(statement);
    }

    public void resetVersion() {
        newVersion.setVersion(oldVersion);
    }

    public void updateEntityVersion() {
        pomController.updateVersion(oldVersion, newVersion);
        pomController.updateArrowColor(isChanged);
        fileViewController.updateVersion(oldVersion, newVersion);
        updateChildrenProperties();
    }

    public void updateEntity() {
        updateEntityVersion();
        updateProperties();
        pomController.updateSeparatorAndSpacing();
    }

    private void updateChildrenProperties() {
        childrenProperty.forEach((artifactId, controller) -> {
            controller.updateVersion(newVersion);
            PomEntity pom = EntityService.getInstance().getPomEntity(artifactId);
            if (pom.isUpdated() && !pom.isChanged() && !pom.isPresentPropertiesUpdatedOrLocked()) {
                pom.resetVersion();
                pom.updateEntityVersion();
            } else if (!pom.isUpdated() && isUpdated() && !controller.isLocked()) { //TODO isUpdated
                pom.updateVersionByState(UpdateVersionStatementEnum.MAJOR);
                pom.updateEntityVersion();
            }
        });
    }

    private boolean isPresentPropertiesUpdatedOrLocked() {
        return presentProperties.values().stream().anyMatch(PropertyEntity::isVersionUpdatedOrLocked);
    }

    private void updateProperties() {
        Set<String> artifactIds = EntityService.getInstance().getPomArtifactIds();

        presentProperties.keySet().stream()
                .filter(a -> !artifactIds.contains(a))
                .collect(Collectors.toSet())
                .forEach(a -> {
                    PropertyEntity property = presentProperties.get(a);
                    pomController.movePropertyFromPresentToAbsent(property.getNode());
                    presentProperties.remove(a);
                    absentProperties.put(a, property);
                    property.setLockButtonDisable(true);
                });

        absentProperties.keySet().stream()
                .filter(artifactIds::contains)
                .collect(Collectors.toSet())
                .forEach(a -> {
                    PropertyEntity property = absentProperties.get(a);
                    pomController.movePropertyFromAbsentToPresent(property.getNode());
                    absentProperties.remove(a);
                    presentProperties.put(a, property);
                    property.setLockButtonDisable(false);
                    EntityService.getInstance().getPomEntity(a).addChildrenProperty(artifactId, property);
                });
    }

    public void updateGroups() {
        pomController.updateGroupDropDownList();
    }

    public void remove() {
        getFilesTabViewController().removeContainer(fileViewContainer);
        getMainViewController().removeContainer(pomContainer);
    }

    public String getFilePath() {
        return filePath;
    }

    public Model getModel() {
        return model;
    }

    // True if oldVersion != newVersion
    public boolean isUpdated() {
        return !oldVersion.toString().equals(newVersion.toString());
    }

    public void addChildrenProperty(String artifactId, PropertyEntity property) {
        childrenProperty.put(artifactId, property);
    }

    public void removeChildrenProperty(String artifactId) {
        childrenProperty.remove(artifactId);
    }

    public boolean isChanged() {
        return isChanged;
    }

    public void saveChanges() {
        oldVersion.setVersion(newVersion);
        model.setVersion(newVersion.toString());
        presentProperties.values().forEach(p -> {
            p.saveChanges();
            model.getProperties().setProperty(p.getArtifactId().concat(".version"), p.getOldVersion());
        });
        isChanged = false;
        getVersionTabViewController().updateVersion(oldVersion, newVersion);
        updateEntityVersion();
    }

    public void addLog(LogElementViewController controller) {
        logs.add(controller);
    }

    public void removeLog(LogElementViewController controller) {
        logs.remove(controller);
    }
}

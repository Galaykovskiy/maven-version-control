package com.kulish.mavenversioncontrol.entities;

import com.kulish.mavenversioncontrol.controllers.PomElementViewController;
import javafx.scene.input.MouseEvent;

public class MovablePomEntity {
    private PomElementViewController pomController;

    private double xOffset = 0;
    private double yOffset = 0;

    public MovablePomEntity(PomElementViewController pomController) {
        this.pomController = pomController;

        this.pomController.setArtifactIdNodePressedHandler(this::handleMousePressed);
        this.pomController.setArtifactIdNodeDraggedHandler(this::handleMouseDragged);
    }

    public void handleMousePressed(MouseEvent event) {
        xOffset = event.getSceneX() - pomController.getPomElementNode().getTranslateX();
        yOffset = event.getSceneY() - pomController.getPomElementNode().getTranslateY();
    }

    public void handleMouseDragged(MouseEvent event) {
        pomController.getPomElementNode().setTranslateX(event.getSceneX() - xOffset);
        pomController.getPomElementNode().setTranslateY(event.getSceneY() - yOffset);
    }
}

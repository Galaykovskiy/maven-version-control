package com.kulish.mavenversioncontrol.enums;

public enum UpdateVersionStatementEnum {
    CRUCIAL,
    MAJOR,
    MINOR
}

package com.kulish.mavenversioncontrol.enums;

public enum ArrowStatementsEnum {
    EMPTY(""),
    NORMAL("➤");

    private final String value;

    private ArrowStatementsEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

package com.kulish.mavenversioncontrol.enums;

import javafx.scene.paint.Color;

public enum VersionColorsEnum {
    BLACK("#000000"),
    GRAY("#DDDDDD"),
    GREEN("#90FF8D"),
    YELLOW("#FFD874"),
    RED("#FF9292");

    private final String color;

    VersionColorsEnum(String hex) {
        color = hex;
    }

    public String getHexCode() {
        return color;
    }

    public Color getColor() {
        return Color.web(color);
    }
}

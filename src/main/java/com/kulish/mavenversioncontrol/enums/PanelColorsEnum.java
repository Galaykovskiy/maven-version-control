package com.kulish.mavenversioncontrol.enums;

public enum PanelColorsEnum {
    GRAY("#CCCCCC"),
    RED("#F09EA7"),
    BLUE("#C7CAFF"),
    GREEN("#C1EBC0"),
    ORANGE("#F6CA94"),
    PURPLE("#CDABEB"),
    YELLOW("#FAFABE");

    private final String color;

    PanelColorsEnum(String hex) {
        color = hex;
    }

    public String getHexCode() {
        return color;
    }
}

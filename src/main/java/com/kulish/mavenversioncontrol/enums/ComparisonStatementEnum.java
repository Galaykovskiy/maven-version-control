package com.kulish.mavenversioncontrol.enums;

public enum ComparisonStatementEnum {
    EQUAL,
    NEWER,
    OLDER,
    UNKNOWN
}

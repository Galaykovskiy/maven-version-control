package com.kulish.mavenversioncontrol.utils;

import javafx.scene.image.Image;

public class ImageLoader {
    private static ImageLoader instance;

    private static final String lockedPath = "https://cdn-icons-png.flaticon.com/128/25/25239.png";
    private static final String unlockedPath = "https://cdn-icons-png.flaticon.com/128/25/25215.png";

   private final Image lockedImage;
   private final Image unlockedImage;

    private ImageLoader() {
        lockedImage = new Image(lockedPath);
        unlockedImage = new Image(unlockedPath);
    }

    public Image getLockedImage() {
        return lockedImage;
    }

    public Image getUnlockedImage() {
        return unlockedImage;
    }

    public static ImageLoader getInstance() {
        if (instance == null) {
            instance = new ImageLoader();
        }
        return instance;
    }
}

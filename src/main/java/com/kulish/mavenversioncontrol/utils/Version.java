package com.kulish.mavenversioncontrol.utils;

import com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum;
import static com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum.EQUAL;
import static com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum.NEWER;
import static com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum.OLDER;
import static com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum.UNKNOWN;
import com.kulish.mavenversioncontrol.enums.UpdateVersionStatementEnum;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Version {
    private int v1;
    private int v2;
    private int v3;
    private String undefined;

    public Version(String version) {
        setVersion(version);
    }

    public void setVersion(Version version) {
        v1 = version.v1;
        v2 = version.v2;
        v3 = version.v3;
        undefined = version.undefined;
    }

    public void setVersion(String version) {
        if (verifyVersionPattern(version)) {
            var arr = parseVersion(version);
            v1 = arr[0];
            v2 = arr[1];
            v3 = arr[2];
        } else {
            this.undefined = version;
        }
    }

    public void updateVersion(UpdateVersionStatementEnum statement) {
        switch (statement) {
            case CRUCIAL -> v1++;
            case MAJOR -> v2++;
            case MINOR -> v3++;
        }
    }

    public int getV1() {
        return v1;
    }

    public int getV2() {
        return v2;
    }

    public int getV3() {
        return v3;
    }

    @Override
    public String toString() {
        if (undefined != null) {
            return undefined;
        }
        return String.join(".", String.valueOf(v1), String.valueOf(v2), String.valueOf(v3));
    }

    public static boolean verifyVersionPattern(String versionText) {
        if (versionText == null) {
            return false;
        }

        Pattern pattern = Pattern.compile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}");
        Matcher matcher = pattern.matcher(versionText);

        return matcher.matches();
    }

    public static boolean verifyVersionPattern(Version version) {
        return verifyVersionPattern(version.toString());
    }

    public static int[] parseVersion(String value) {
        return Arrays.stream(value.split("\\."))
                .mapToInt(Integer::parseInt)
                .toArray();
    }

    public static ComparisonStatementEnum compareVersions(Version oldVersion, Version newVersion) {
        return compareVersions(oldVersion.toString(), newVersion.toString());
    }

    public static ComparisonStatementEnum compareVersions(String oldVersion, String newVersion) {
        if (!verifyVersionPattern(oldVersion) && !verifyVersionPattern(newVersion)) {
            return UNKNOWN;
        }

        int[] arr1 = parseVersion(oldVersion);
        int[] arr2 = parseVersion(newVersion);

        for (int i = 0; i < Math.min(arr1.length, arr2.length); i++) {
            if (arr1[i] > arr2[i]) {
                return OLDER;
            } else if (arr1[i] < arr2[i]) {
                return NEWER;
            }
        }
        return EQUAL;
    }
}

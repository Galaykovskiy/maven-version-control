package com.kulish.mavenversioncontrol.controllers;

import com.kulish.mavenversioncontrol.enums.ArrowStatementsEnum;
import com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum;
import com.kulish.mavenversioncontrol.enums.VersionColorsEnum;
import com.kulish.mavenversioncontrol.service.GroupsService;
import com.kulish.mavenversioncontrol.utils.Version;
import static com.kulish.mavenversioncontrol.utils.Version.compareVersions;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

public class PomElementViewController {
    @FXML
    private VBox pomElementNode;
    @FXML
    private VBox artifactIdNode;
    @FXML
    private VBox versionNode;
    @FXML
    private ComboBox<String> groupDropDown;
    @FXML
    private Label artifactId;
    @FXML
    private Label oldVersion;
    @FXML
    private Label arrow;
    @FXML
    private Label newVersion;
    @FXML
    private VBox propertiesSection;
    @FXML
    private VBox presentNodes;
    @FXML
    private Separator separator;
    @FXML
    private VBox absentNodes;

    public void initialize() {
        setDefaultValues();
        updateGroupDropDownList();
    }

    public void updateSeparatorAndSpacing() {
        boolean isEmpty = presentNodes.getChildren().isEmpty() || absentNodes.getChildren().isEmpty();
        separator.setVisible(!isEmpty);
        propertiesSection.setSpacing(isEmpty ? 0.0 : 10.0);
    }

    public void updateArrowColor(boolean isChanged) {
        if (isChanged) {
            arrow.setTextFill(VersionColorsEnum.GREEN.getColor());
        } else {
            arrow.setTextFill(VersionColorsEnum.BLACK.getColor());
        }
    }

    public void updateVersion(Version oldVersion, Version newVersion) {
        ComparisonStatementEnum result = compareVersions(oldVersion, newVersion);
        arrow.setText(ArrowStatementsEnum.NORMAL.getValue());
        this.oldVersion.setText(oldVersion.toString());
        this.newVersion.setText(newVersion.toString());

        if (result == ComparisonStatementEnum.NEWER) {
            this.newVersion.setTextFill(VersionColorsEnum.GREEN.getColor());
        } else if (result == ComparisonStatementEnum.OLDER) {
            this.newVersion.setTextFill(VersionColorsEnum.RED.getColor());
        } else if (result == ComparisonStatementEnum.EQUAL || result == ComparisonStatementEnum.UNKNOWN) {
            setDefaultValues();
        } else {
            this.newVersion.setTextFill(VersionColorsEnum.YELLOW.getColor());
        }
    }

    private void setDefaultValues() {
        arrow.setText(ArrowStatementsEnum.EMPTY.getValue());
        newVersion.setText("");
    }

    public void updateGroupDropDownList() {
        groupDropDown.setItems(FXCollections.observableArrayList(
                GroupsService.getInstance().getGroups()
                        .keySet()
                        .stream()
                        .sorted()
                        .collect(Collectors.toList())));
    }

    public void setBackgroundColor(String group) {
        artifactIdNode.setStyle(String.format("-fx-background-color: %s;", GroupsService.getInstance().getGroups().get(group)));
    }

    public void addVersionNodeHandler(EventHandler<? super MouseEvent> event) {
        versionNode.setOnMouseClicked(event);
    }

    public void setArtifactId(String artifactId) {
        this.artifactId.setText(artifactId);
    }

    public void setOldVersion(Version oldVersion) {
        this.oldVersion.setText(oldVersion.toString());
    }

    public void setGroupDropDownValue(String group) {
        groupDropDown.setValue(group);
    }

    public String getGroupDropDownValue() {
        return groupDropDown.getValue();
    }

    public void setGroupDropDownHandler(EventHandler<ActionEvent> event) {
        groupDropDown.setOnAction(event);
    }

    public void setArtifactIdNodePressedHandler(EventHandler<? super MouseEvent> event) {
        artifactIdNode.setOnMousePressed(event);
    }

    public void setArtifactIdNodeDraggedHandler(EventHandler<? super MouseEvent> event) {
        artifactIdNode.setOnMouseDragged(event);
    }

    public void addPropertyToAbsentNode(Node node) {
        absentNodes.getChildren().add(node);
    }

    public void addPropertyToPresentNode(Node node) {
        presentNodes.getChildren().add(node);
    }

    public void movePropertyFromAbsentToPresent(Node node) {
        absentNodes.getChildren().remove(node);
        presentNodes.getChildren().add(node);
    }

    public void movePropertyFromPresentToAbsent(Node node) {
        presentNodes.getChildren().remove(node);
        absentNodes.getChildren().add(node);
    }

    public VBox getPomElementNode() {
        return pomElementNode;
    }
}
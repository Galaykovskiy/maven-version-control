package com.kulish.mavenversioncontrol.controllers.tabs;

import com.kulish.mavenversioncontrol.controllers.IController;
import com.kulish.mavenversioncontrol.enums.ArrowStatementsEnum;
import com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum;
import com.kulish.mavenversioncontrol.enums.VersionColorsEnum;
import com.kulish.mavenversioncontrol.service.ControllerService;
import com.kulish.mavenversioncontrol.service.EntityService;
import com.kulish.mavenversioncontrol.utils.Version;
import static com.kulish.mavenversioncontrol.utils.Version.compareVersions;
import static com.kulish.mavenversioncontrol.utils.Version.verifyVersionPattern;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class VersionEditorTabViewController implements IController {
    @FXML
    private VBox updateBox;
    @FXML
    private HBox versionBox;
    @FXML
    private Label oldVersion;
    @FXML
    private Label arrow;
    @FXML
    private Label newVersion;
    @FXML
    private VBox versionPanel;
    @FXML
    private Label artifactId;
    @FXML
    private Spinner<Integer> spinner1;
    @FXML
    private Spinner<Integer> spinner2;
    @FXML
    private Spinner<Integer> spinner3;
    @FXML
    private Button applyButton;
    @FXML
    private Button resetButton;

    private SpinnerValueFactory<Integer> valueFactory1;
    private SpinnerValueFactory<Integer> valueFactory2;
    private SpinnerValueFactory<Integer> valueFactory3;

    public void initialize() {
        ControllerService.getInstance().addController(getClass(), this);
        versionPanel.setVisible(false);
        setupSpinners();
    }

    private void setupSpinners() {
        valueFactory1 = setupSpinner(spinner1, (observableValue, oldValue, newValue) -> {
            if (newValue > oldValue) {
                valueFactory2.setValue(0);
                valueFactory3.setValue(0);
            }
        });
        valueFactory2 = setupSpinner(spinner2, (observableValue, oldValue, newValue) -> {
            if (newValue > oldValue) {
                valueFactory3.setValue(0);
            }
        });
        valueFactory3 = setupSpinner(spinner3, (observableValue, oldValue, newValue) -> {
        });
    }

    private SpinnerValueFactory<Integer> setupSpinner(Spinner<Integer> spinner, ChangeListener<Integer> listener) {
        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 999);
        spinner.setValueFactory(valueFactory);
        spinner.editorProperty().get().setAlignment(Pos.CENTER);
        valueFactory.valueProperty().addListener(listener);

        return valueFactory;
    }

    public void setArtifactIdText(String artifactIdText) {
        artifactId.setText(artifactIdText);
    }

    public void setVisible(boolean isVisible) {
        versionPanel.setVisible(isVisible);
    }

    public void updateVersion(Version oldVersion, Version newVersion) {
        this.oldVersion.setText(oldVersion.toString());
        arrow.setText(ArrowStatementsEnum.NORMAL.getValue());
        this.newVersion.setText(newVersion.toString());
        updateSpinner(newVersion);

        ComparisonStatementEnum result = compareVersions(oldVersion, newVersion);
        if (result == ComparisonStatementEnum.NEWER) {
            this.newVersion.setTextFill(VersionColorsEnum.GREEN.getColor());
        } else if (result == ComparisonStatementEnum.OLDER) {
            this.newVersion.setTextFill(VersionColorsEnum.RED.getColor());
        } else if (result == ComparisonStatementEnum.EQUAL) {
            setDefaultState();
            this.oldVersion.setTextFill(VersionColorsEnum.BLACK.getColor());
        } else {
            this.newVersion.setTextFill(VersionColorsEnum.YELLOW.getColor());
        }
    }

    private void updateSpinner(Version newVersion) {
        if (verifyVersionPattern(newVersion)) {
            valueFactory1.setValue(newVersion.getV1());
            valueFactory2.setValue(newVersion.getV2());
            valueFactory3.setValue(newVersion.getV3());

            updateBox.setVisible(true);
            versionBox.setSpacing(5);
        } else {
            updateBox.setVisible(false);
            versionBox.setSpacing(0);
        }
    }

    public void setDefaultState() {
        arrow.setText(ArrowStatementsEnum.EMPTY.getValue());
        newVersion.setText("");
    }

    public String getVersion() {
        return String.format("%d.%d.%d", valueFactory1.getValue(), valueFactory2.getValue(), valueFactory3.getValue());
    }

    public void setApplyButtonHandler(EventHandler<? super MouseEvent> event) {
        applyButton.setOnMouseClicked(event);
    }

    public void setResetButtonHandler(EventHandler<? super MouseEvent> event) {
        resetButton.setOnMouseClicked(event);
    }

    @FXML
    private void onSaveChangesClicked() {
        EntityService.getInstance().saveAllChanges();
    }
}

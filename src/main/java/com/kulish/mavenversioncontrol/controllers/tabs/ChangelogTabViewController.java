package com.kulish.mavenversioncontrol.controllers.tabs;

import com.kulish.mavenversioncontrol.controllers.IController;
import com.kulish.mavenversioncontrol.service.ControllerService;
import com.kulish.mavenversioncontrol.service.EntityService;
import com.kulish.mavenversioncontrol.utils.Version;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.Set;

public class ChangelogTabViewController implements IController {
    @FXML
    private VBox changelogTab;
    @FXML
    private Label artifactId;
    @FXML
    private VBox scrollPane;
    @FXML
    private Label version;
    @FXML
    private TextField inputField;

    public void initialize() {
        ControllerService.getInstance().addController(getClass(), this);

        inputField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                try {
                    onAddLogClicked();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    private void createLogElement() throws IOException {
        FXMLLoader loader = new FXMLLoader(LogElementViewController.class.getResource("changelog-element-view.fxml"));
        HBox container = loader.load();
        scrollPane.getChildren().add(container);

        LogElementViewController controller = loader.getController();
        controller.setText(String.format("* %s", inputField.getText()));
        EntityService.getInstance().getCurrentPomEntity().addLog(controller);
    }

    @FXML
    private void onAddLogClicked() throws IOException {
        if (inputField.getText().equals(""))
            return;
        createLogElement();
        inputField.setText("");
    }

    public void updateScrollPane(Set<LogElementViewController> logs) {
        scrollPane.getChildren().clear();
        logs.forEach(l -> scrollPane.getChildren().add(l.getNode()));
    }

    public void setArtifactId(String artifactId) {
        this.artifactId.setText(artifactId);
    }

    public void setVersion(Version version) {
        this.version.setText(version.toString());
    }

    public void setVisible(boolean isVisible) {
        changelogTab.setVisible(isVisible);
    }
}

package com.kulish.mavenversioncontrol.controllers.tabs;

import com.kulish.mavenversioncontrol.enums.ArrowStatementsEnum;
import com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum;
import static com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum.EQUAL;
import static com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum.NEWER;
import static com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum.OLDER;
import static com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum.UNKNOWN;
import com.kulish.mavenversioncontrol.enums.VersionColorsEnum;
import static com.kulish.mavenversioncontrol.service.ControllerService.Controllers.getChangelogTabViewController;
import static com.kulish.mavenversioncontrol.service.ControllerService.Controllers.getVersionTabViewController;
import com.kulish.mavenversioncontrol.service.EntityService;
import com.kulish.mavenversioncontrol.utils.Version;
import static com.kulish.mavenversioncontrol.utils.Version.compareVersions;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class FileViewController {
    @FXML
    private Label artifactId;
    @FXML
    private Label oldVersion;
    @FXML
    private Label arrow;
    @FXML
    private Label newVersion;

    public void initialize() {
        setDefaultState();
    }

    public void setArtifactId(String artifactId) {
        this.artifactId.setText(artifactId);
    }

    public void setOldVersion(String version) {
        oldVersion.setText(version);
    }

    public void updateVersion(Version oldVersion, Version newVersion) {
        ComparisonStatementEnum result = compareVersions(oldVersion, newVersion);
        arrow.setText(ArrowStatementsEnum.NORMAL.getValue());
        this.newVersion.setText(newVersion.toString());
        if (result == NEWER) {
            this.newVersion.setTextFill(VersionColorsEnum.GREEN.getColor());
        } else if (result == OLDER) {
            this.newVersion.setTextFill(VersionColorsEnum.RED.getColor());
        } else if (result == EQUAL || result == UNKNOWN) {
            setDefaultState();
        } else {
            this.newVersion.setTextFill(VersionColorsEnum.YELLOW.getColor());
        }
    }

    private void setDefaultState() {
        arrow.setText(ArrowStatementsEnum.EMPTY.getValue());
        newVersion.setText("");
    }

    @FXML
    private void onRemoveClicked() {
        EntityService.getInstance().removePomEntity(artifactId.getText());
        getVersionTabViewController().setVisible(false);
        getChangelogTabViewController().setVisible(false);
    }
}

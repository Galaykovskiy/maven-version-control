package com.kulish.mavenversioncontrol.controllers.tabs;

import com.kulish.mavenversioncontrol.enums.PanelColorsEnum;
import static com.kulish.mavenversioncontrol.service.GroupsService.DEFAULT_GROUP;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

public class GroupViewController {
    @FXML
    private Button removeButton;
    @FXML
    private Pane pane;
    @FXML
    private Label groupName;

    private PanelColorsEnum color;

    public void setBackgroundColor(PanelColorsEnum color) {
        this.color = color;
        pane.setStyle(String.format("-fx-background-color: %s;", color.getHexCode()));
    }

    public void setGroupTextName(String groupTextName) {
        groupName.setText(groupTextName);
        if (!groupTextName.equals(DEFAULT_GROUP))
            return;
        removeButton.setVisible(false);
    }

    public Button getRemoveButton() {
        return removeButton;
    }

    public String getGroupNameText() {
        return groupName.getText();
    }

    public PanelColorsEnum getColor() {
        return color;
    }
}

package com.kulish.mavenversioncontrol.controllers.tabs;

import com.kulish.mavenversioncontrol.controllers.IController;
import com.kulish.mavenversioncontrol.service.ControllerService;
import com.kulish.mavenversioncontrol.service.EntityService;
import com.kulish.mavenversioncontrol.service.PomService;
import com.kulish.mavenversioncontrol.view.Alert;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.maven.model.Model;

public class FilesTabViewController implements IController {
    @FXML
    private Pane scrollPane;

    private BooleanProperty mainWindowHandler;

    public void initialize() {
        ControllerService.getInstance().addController(getClass(), this);
        mainWindowHandler = new SimpleBooleanProperty(false);
    }

    @FXML
    protected void onAddPomFileClicked() {
        mainWindowHandler.setValue(true);
        chooseFile();
        mainWindowHandler.setValue(false);
    }

    private void chooseFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose POM.XML file");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Pom files", "pom.xml"));
        // Set the fileChooser to directory selection mode
        Stage stage = new Stage();

        File selectedFile = fileChooser.showOpenDialog(stage);
        if (selectedFile == null) {
            return;
        }
        if (selectedFile.getName().toLowerCase().endsWith(".xml")
                && !EntityService.getInstance().isPomFileExisting(selectedFile)) {
            PomService.getInstance().readPomFile(selectedFile);
        } else if (EntityService.getInstance().isPomFileExisting(selectedFile)) {
            Alert.show(String.format("'%s' was added", selectedFile.getPath()));
        } else {
            Alert.show("Allow to add only 'pom.xml'");
        }
    }

    @FXML
    private void onAddDirectoryClicked() {
        mainWindowHandler.setValue(true);
        chooseDirectory();
        mainWindowHandler.setValue(false);
    }

    private void chooseDirectory() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Choose working directory");
        Stage stage = new Stage();

        File selectedDir = directoryChooser.showDialog(stage);
        if (selectedDir == null) {
            return;
        }

        findPomFiles(selectedDir);
    }

    /**
     * Recursively finds all "pom.xml" files in the given directory and its subdirectories.
     * It uses the Apache Maven Model (POM) to read and parse the contents of each "pom.xml" file.
     *
     * @param dir The directory in which to search for "pom.xml" files.
     * @return A list of Model objects representing the parsed contents of each "pom.xml" file found.
     */
    private List<Model> findPomFiles(File dir) {
        File[] files = dir.listFiles();
        List<Model> models = new LinkedList<>();
        if (files == null) {
            return Collections.emptyList();
        }
        Arrays.stream(files).forEach(file -> {
            if (file.isDirectory()) {
                models.addAll(findPomFiles(file));
            } else if (file.isFile() && file.getName().equalsIgnoreCase("pom.xml")
                    && !EntityService.getInstance().isPomFileExisting(file)) {
                PomService.getInstance().readPomFile(file);
            }
        });
        return models;
    }

    public void onMainWindowActiveHandler(ChangeListener<Boolean> changeListener) {
        mainWindowHandler.addListener(changeListener);
    }

    public void addContainer(Node node) {
        scrollPane.getChildren().add(node);
    }

    public void removeContainer(Node node) {
        scrollPane.getChildren().remove(node);
    }
}

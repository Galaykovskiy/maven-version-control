package com.kulish.mavenversioncontrol.controllers.tabs;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

public class LogElementViewController {
    @FXML
    private HBox logElement;
    @FXML
    private Label text;

    public void setText(String text) {
        this.text.setText(text);
    }

    public HBox getNode() {
        return logElement;
    }

    public void addLogElementHandler(EventHandler<? super MouseEvent> event) {
        logElement.setOnMouseClicked(event);
    }
}

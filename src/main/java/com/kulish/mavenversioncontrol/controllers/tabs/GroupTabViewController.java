package com.kulish.mavenversioncontrol.controllers.tabs;

import com.kulish.mavenversioncontrol.enums.PanelColorsEnum;
import com.kulish.mavenversioncontrol.service.EntityService;
import com.kulish.mavenversioncontrol.service.GroupsService;
import com.kulish.mavenversioncontrol.view.Alert;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Pair;

public class GroupTabViewController {
    @FXML
    private HBox inputBox;
    @FXML
    private VBox usedGroupsContainer;
    @FXML
    private TextField inputField;
    @FXML
    private VBox unusedGroupsContainer;

    private final Map<String, String> groups = GroupsService.getInstance().getGroups();

    private LinkedList<Pair<HBox, GroupViewController>> unusedGroups;
    private Map<HBox, GroupViewController> usedGroups;

    public void initialize() {
        unusedGroups = new LinkedList<>();
        usedGroups = new HashMap<>();
        for (PanelColorsEnum colorEnum : PanelColorsEnum.values()) {
            unusedGroups.addLast(initializeGroup(colorEnum));
        }
        inputField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER)
                onAddGroupClicked();
        });

        GroupsService.getInstance().getGroups().forEach((groupName, color) -> addGroup(groupName));
    }

    @FXML
    private void onAddGroupClicked() {
        String inputName = inputField.getText().toUpperCase();
        if (inputName.equals("")) {
            Alert.show("Group name is empty");
        } else if (groups.containsKey(inputName)) {
            Alert.show(String.format("'%s' is already exist!", inputName));
        } else {
            addGroup(inputName);
        }
        inputField.setText("");
        EntityService.getInstance().updateGroups();
    }

    private void addGroup(String groupName) {
        Pair<HBox, GroupViewController> group = unusedGroups.getFirst();
        group.getValue().setGroupTextName(groupName);
        group.getKey().setVisible(true);

        usedGroupsContainer.getChildren().add(group.getKey());
        unusedGroupsContainer.getChildren().remove(group.getKey());

        usedGroups.put(group.getKey(), group.getValue());
        unusedGroups.removeFirst();

        groups.put(groupName, group.getValue().getColor().getHexCode());
        verifyIsGroupMax();
    }

    private Pair<HBox, GroupViewController> initializeGroup(PanelColorsEnum color) {
        try {
            FXMLLoader loader = new FXMLLoader(GroupViewController.class.getResource("group-view.fxml"));
            HBox container = loader.load();
            unusedGroupsContainer.getChildren().add(container);

            GroupViewController controller = loader.getController();
            Pair<HBox, GroupViewController> group = new Pair<>(container, controller);

            controller.setBackgroundColor(color);
            controller.getRemoveButton().setOnMouseClicked(event -> {
                removeGroup(controller.getGroupNameText(), group);
                verifyIsGroupMax();
                EntityService.getInstance().updateGroups();
            });

            container.setVisible(false);
            return group;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void removeGroup(String groupName, Pair<HBox, GroupViewController> group) {
        groups.remove(groupName);
        usedGroups.remove(group.getKey());
        unusedGroups.addFirst(group);

        group.getKey().setVisible(false);
        usedGroupsContainer.getChildren().remove(group.getKey());
        unusedGroupsContainer.getChildren().add(group.getKey());
    }

    public void verifyIsGroupMax() {
        inputBox.setVisible(!unusedGroups.isEmpty());
    }
}

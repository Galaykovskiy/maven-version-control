package com.kulish.mavenversioncontrol.controllers;

import com.kulish.mavenversioncontrol.controllers.tabs.ChangelogTabViewController;
import com.kulish.mavenversioncontrol.controllers.tabs.FilesTabViewController;
import com.kulish.mavenversioncontrol.controllers.tabs.GroupViewController;
import com.kulish.mavenversioncontrol.controllers.tabs.VersionEditorTabViewController;
import com.kulish.mavenversioncontrol.service.ControllerService;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class MainViewController implements IController {
    @FXML
    private HBox mainWindow;
    @FXML
    private Pane pane;
    @FXML
    private TabPane tabPane;
    @FXML
    private Tab versionsTab;
    @FXML
    private Tab groupsTab;
    @FXML
    private Tab filesTab;
    @FXML
    private Tab changelogTab;

    public void initialize() throws Exception {
        ControllerService.getInstance().addController(this.getClass(), this);

        loadVersionsTab();
        loadFilesTab();
        loadGroupsTab();
        loadChangelogTab();
    }

    private void loadVersionsTab() throws Exception {
        FXMLLoader loader = new FXMLLoader(VersionEditorTabViewController.class.getResource("version-editor-tab-view.fxml"));
        VBox container = loader.load();
        versionsTab.setContent(container);
    }

    private void loadGroupsTab() throws Exception {
        FXMLLoader loader = new FXMLLoader(GroupViewController.class.getResource("groups-tab-view.fxml"));
        VBox container = loader.load();
        groupsTab.setContent(container);
    }

    private void loadFilesTab() throws IOException {
        FXMLLoader loader = new FXMLLoader(FilesTabViewController.class.getResource("files-tab-view.fxml"));
        VBox container = loader.load();
        filesTab.setContent(container);

        FilesTabViewController controller = loader.getController();
        controller.onMainWindowActiveHandler((observableValue, oldValue, newValue) -> mainWindow.setDisable(newValue));
    }

    private void loadChangelogTab() throws IOException {
        FXMLLoader loader = new FXMLLoader(ChangelogTabViewController.class.getResource("changelog-tab-view.fxml"));
        VBox container = loader.load();
        changelogTab.setContent(container);
    }

    public void selectVersionTab() {
        tabPane.getSelectionModel().select(versionsTab);
    }

    public void addContainer(Node node) {
        pane.getChildren().add(node);
    }

    public void removeContainer(Node node) {
        pane.getChildren().remove(node);
    }
}
package com.kulish.mavenversioncontrol.controllers;

import com.kulish.mavenversioncontrol.enums.ArrowStatementsEnum;
import com.kulish.mavenversioncontrol.enums.ComparisonStatementEnum;
import com.kulish.mavenversioncontrol.enums.VersionColorsEnum;
import com.kulish.mavenversioncontrol.utils.ImageLoader;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class PropertyViewController {
    @FXML
    private Button lockButton;
    @FXML
    private Label artifactId;
    @FXML
    private Label oldVersion;
    @FXML
    private Label arrow;
    @FXML
    private Label newVersion;

    private ImageView lockedImageView;
    private ImageView unlockedImageView;

    public void initialize() {
        lockedImageView = instantiateImageView(ImageLoader.getInstance().getLockedImage());
        unlockedImageView = instantiateImageView(ImageLoader.getInstance().getUnlockedImage());
    }

    private ImageView instantiateImageView(Image image) {
        double fitSize = 16.0;

        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(fitSize);
        imageView.setFitWidth(fitSize);

        return imageView;
    }

    public void updateLockButtonGraphic(boolean isLocked) {
        lockButton.setGraphic(isLocked ? lockedImageView : unlockedImageView);
    }

    private void setEmptyState() {
        arrow.setText(ArrowStatementsEnum.EMPTY.getValue());
        newVersion.setText("");
    }

    public void setNormalState(ComparisonStatementEnum result) {
        arrow.setText(ArrowStatementsEnum.NORMAL.getValue());
        arrow.setTextFill(VersionColorsEnum.BLACK.getColor());
        if (result == ComparisonStatementEnum.NEWER) {
            newVersion.setTextFill(VersionColorsEnum.GREEN.getColor());
        } else if (result == ComparisonStatementEnum.OLDER) {
            newVersion.setTextFill(VersionColorsEnum.RED.getColor());
        } else {
            setEmptyState();
        }
    }

    public void setStrickenState() {
        arrow.setTextFill(VersionColorsEnum.GRAY.getColor());
        newVersion.setTextFill(VersionColorsEnum.GRAY.getColor());
    }

    public void setArtifactId(String artifactId) {
        this.artifactId.setText(artifactId);
    }

    public void setOldVersion(String version) {
        oldVersion.setText(version);
    }

    public void setNewVersion(String version) {
        newVersion.setText(version);
    }

    public void setLockButtonDisable(boolean isDisable) {
        lockButton.setDisable(isDisable);
    }

    public void setOnLockButtonHandler(EventHandler<MouseEvent> event) {
        lockButton.setOnMouseClicked(event);
    }
}

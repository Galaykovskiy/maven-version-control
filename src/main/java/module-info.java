module com.kulish.mavenversioncontrol {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.ikonli.javafx;
    requires maven.model;
    requires plexus.utils;

    opens com.kulish.mavenversioncontrol to javafx.fxml;
    exports com.kulish.mavenversioncontrol;
    exports com.kulish.mavenversioncontrol.service;
    opens com.kulish.mavenversioncontrol.service to javafx.fxml;
    exports com.kulish.mavenversioncontrol.controllers;
    opens com.kulish.mavenversioncontrol.controllers to javafx.fxml;
    exports testscenes;
    opens testscenes to javafx.fxml;
    exports com.kulish.mavenversioncontrol.controllers.tabs;
    opens com.kulish.mavenversioncontrol.controllers.tabs to javafx.fxml;
}